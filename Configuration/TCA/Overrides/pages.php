<?php
defined('TYPO3_MODE') or die();

$additionalColumns = [
    'caw_enable' => array(
        'exclude' => 1,
        'label' => 'LLL:EXT:contentadditionalwrap/Resources/Private/Language/locallang.xlf:plugin.mdy.contentadditionalwrap.be.string.caw_enable.label',
        'config' => array(
            'type' => 'check',
        )
    ),
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $additionalColumns);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages',
    '--palette--;LLL:EXT:contentadditionalwrap/Resources/Private/Language/locallang.xlf:plugin.mdy.contentadditionalwrap.be.string.palette.pages.title;contentadditionalwrappages',
    '1',
    'after:nav_title'
);

$GLOBALS['TCA']['pages']['palettes']['contentadditionalwrappages'] = array(
    'showitem' => 'caw_enable'
);