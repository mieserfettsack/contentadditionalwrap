<?php
defined('TYPO3_MODE') or die();

$additionalColumns = array(
    'caw_color' => [
        'label' => 'LLL:EXT:contentadditionalwrap/Resources/Private/Language/locallang.xlf:plugin.mdy.contentadditionalwrap.be.string.caw_color.label',
        'config' => [
            'type' => 'input',
            'renderType' => 'colorpicker',
        ],
    ],
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $additionalColumns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tt_content',
    '--palette--;LLL:EXT:contentadditionalwrap/Resources/Private/Language/locallang.xlf:plugin.mdy.contentadditionalwrap.be.string.palette.ttcontent.title;contentadditionalwrapcontent',
    '',
    'after:linkToTop'
);

$GLOBALS['TCA']['tt_content']['palettes']['contentadditionalwrapcontent'] = array(
    'showitem' => 'caw_color'
);