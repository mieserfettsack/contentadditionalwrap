<?php
defined('TYPO3_MODE') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('contentadditionalwrap', 'Configuration/TypoScript', 'Additional wrap for tt_content');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('contentadditionalwrap', 'Configuration/TypoScript/Styling', 'Additional wrap for tt_content - CSS (optional)');