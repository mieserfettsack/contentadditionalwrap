plugin {
    mdy_contentadditionalwrap {
        _CSS_DEFAULT_STYLE (
            .frame-outer-odd .frame {
                background-color: {$plugin.mdy.contentadditionalwrap.css.odd.color};
            }

            .frame-outer-even .frame {
                background-color: {$plugin.mdy.contentadditionalwrap.css.even.color};
            }

            .frame {
                overflow: hidden;
            }

            .frame-outer-override-bg .frame {
                background-color: transparent;
            }
        )
    }
}