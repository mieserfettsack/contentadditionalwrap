<?php
$EM_CONF[$_EXTKEY] = array(
    'title' => 'contentadditionalwrap',
    'description' => 'Adds extra wraps to each content element to use odd/even and background colors.',
    'category' => 'fe',
    'author' => 'Christian Stern',
    'author_email' => 'christian.stern@pornofilm-produzent.de',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'version' => '1.2.0',
    'constraints' => array(
        'depends' => array(
            'typo3' => '6.2.0-8.7.99',
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
);

$GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields'] .= ($GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields'] ? ',' : '') . 'caw_enable';